<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'UserApiController@login');

Route::group(['middleware' => 'auth.jwt'], function () {

    Route::get('logout', 'UserApiController@logout');
    Route::get('user-info', 'UserApiController@getUser');
});
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
