<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= User::create([
            'username' => 'admin',
            'mobile_number' => '01141918237',
            'password' => Hash::make(123456),
            'role' => '1',
          
        ]);
    }
}
